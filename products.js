export class Products {
  constructor() {
    this.products = [];
  }
  getByProductNameIndex(productName) {
    this.products.findIndex((product) => product.name == productName);
  }
  add(productName, quantity) {
    if (typeof productName != "string")
      throw "Product name should be type of string";
    else if (typeof quantity < 0) throw "quantity at least should be 1";
    let productIndex = this.getByProductNameIndex(productName);
    if (productIndex == -1) {
      this.products.push({
        productName,
        quantity,
      });
    }
    this.products[productIndex].quantity += quantity;
  }
  remove(productName) {
    if (typeof productName != "string")
      throw "Product name should be type of string";
    let productIndex = this.getByProductNameIndex(productName);
    if (productIndex == -1) throw "Given product name is not exist";
    this.products.splice(productIndex, 1);
  }
  list() {
    return this.products;
  }
}
